var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var settingsSchema = new mongoose.Schema({
    key: {
        type: String,
        required: true
    },
    value: {
        type: String
    }
});
module.exports = mongoose.model('settings', settingsSchema);