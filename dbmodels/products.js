var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    price:{
        type:Number,
        required: true
    },
    category_id:{
        type: Schema.Types.ObjectId,
        ref: 'categories',
        required:true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    last_updated: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('products', productSchema);