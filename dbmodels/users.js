var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var usersSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password:{
        type:String,
        required: true
    },
    isVarified: { type: Number, default: 0, enum: [0, 1] },
    role: { type: Number, default: 1, enum: [0,1,2,3,4,5,6] }, // 0 for super admin and rest is roles -1
    company_id:{
        type: Schema.Types.ObjectId,
        ref: 'company',
        required:true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    last_updated: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('users', usersSchema);