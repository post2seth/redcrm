var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    details: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    last_updated: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('company', companySchema);