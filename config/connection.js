var mongoose = require('mongoose');
var db = mongoose.connection;

db.on('error', console.error);

db.once('open', function() {
  	console.log("mongoose connected");
});

var settings = require('./settings.js');

if(settings.dev===true){
	var devSettings=require('./dev_dbsettings.js');
	mongoose.connect(devSettings.connection+"/"+devSettings.database);
	//console.log(devSettings.connection+"/"+devSettings.db);
}else{
	var dbSettings=require('./dbsettings.js');
	mongoose.connect(dbSettings.connection+"/"+dbSettings.database);
}
module.exports = db;