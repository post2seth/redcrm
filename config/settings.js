var settings = {
	"dev": true,
	roles:{
		0:"superuser",
		1:"admin",
		2:"manager",
		3:"employee",
		4:"guest",
		5:"additional guest"
	}
};
module.exports = settings;