// main app started here
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var session = require('express-session');
var logger = require('winston');
var i18n = require("i18n");
i18n.configure({
    locales: ['en', 'de', 'ar'],
    directory: __dirname + '/locales',
    register: global
});

// adding all route file
var routes = require('./routes/index');
var users = require('./routes/users');
var superuser = require('./routes/superuser');
var admin = require('./routes/admin');
var notauth = require('./routes/notauth');
var signup = require('./routes/signup');
var forget = require('./routes/forget');
var manager = require('./routes/manager');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(session({
    secret: '!reDCrM$'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// Setting up the locale
setLocale('en');


// added connections & settings
var connection = require('./config/connection');
var settings = require("./config/settings.js");


// deinfing routes.
app.use('/', routes);
app.use('/signup', signup);
app.use('/forget', forget);
app.use('/notauth', notauth);
// authenticate is the funciton which authenticate the admin user & super user
// these three routes are the dashboard routes for the users, now you can append another routes into that
app.use('/usr', isAuthenticated(5), users);
app.use('/adm', isAuthenticated(2), admin);
app.use('/manager', isAuthenticated(3), manager);
app.use('/su', isAuthenticated(1), superuser);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// production error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


function isAuthenticated(roleRequired) {
    return function(req, res, next) {
        if (req.session.role > roleRequired) {
            res.redirect('/notauth');
            return;
        }
        if (req.session.authorized !== undefined) {
            return next();
        }
        // IF A USER ISN'T LOGGED IN, THEN REDIRECT TO HOME PAGE WHICH IS LOGIN PAGE
        res.redirect('/');
        return;
    };
}

module.exports = app;