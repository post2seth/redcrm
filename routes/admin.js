var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    $data = {
        type: "admin",
        title: "Admin Page",
        uuid: req.session.uuid,
        hascategory: true
    };
    res.render('admin', $data);
});
module.exports = router;