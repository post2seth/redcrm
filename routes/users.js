var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
	$data = {
			type:"guest",
			title:"User Page",
			uuid:req.session.uuid
		};
	    res.render('users', $data);
});

module.exports = router;