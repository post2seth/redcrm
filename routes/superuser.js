var express = require('express');
var Users = require('../dbmodels/users.js');
var Company = require('../dbmodels/company.js');
var md5 = require('js-md5');
var settings = require('../config/settings.js');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    Users.find({}).populate('company_id')
        .exec(function(err, users) {
            $data = {
                role: req.session.role,
                title: "Super user Page",
                uuid: req.session.uuid,
                userlist: users,
                roles:settings.roles
            };
            res.render('superuser', $data);
        });
});

router.get('/adduser', function(req, res, next) {
    renderUserPage(req.session.uuid, "", res);
});

router.get('/edituser/:id', function(req, res, next) {
    
    renderUserPage(req.session.uuid, "", res);
});

router.post('/adduser', function(req, res, next) {
    var error = "";
    if (req.body.username == "" || req.body.password == "") {
        error = __n("emptyfields");
        renderUserPage(req.session.uuid, error, res);
        return;
    }
    if (req.body.action.toString().toLowerCase() == "save") {
        Users.findOne({ username: req.body.username, password: md5(req.body.password) }, function(err, userdetails) {
            if (err) {
                error = "Something went wrong";
                renderUserPage(req.session.uuid, error, res);
                return;
            }
            if (userdetails === null) {
                var user = new Users();
                user.username = req.body.username;
                user.password = md5(req.body.password);
                user.role = req.body.role;
                if(req.body.isVarified!=undefined){
                	user.isVarified =  1;	
                }
                user.company_id = req.body.cname;
                user.save(function(err) {
                    if (err) {
                        error = "Something went wrong";
                    } else {
                        error = "Added Sucessfully!";
                    }
                    renderUserPage(req.session.uuid, error, res);

                });
            } else {
                error = __n("alreadyexistsmsg");
                renderUserPage(req.session.uuid, error, res);
            }

        });
    }
});


router.get('/addcompany', function(req, res, next) {
    $data = {
        type: "su",
        title: "Add Company Page",
        uuid: req.session.uuid,
        error: ""
    };
    res.render('./popups/addcompany', $data);
});

router.post('/addcompany', function(req, res, next) {
    var error = "";
    if (req.body.cname == "") {
        error = __n("emptyfields");
    }
    if (error == "") {
        if (req.body.action.toString().toLowerCase() == "save") {
            Company.findOne({ name: req.body.cname }, function(err, companydetails) {
                if (err) {
                    error = "Something went wrong";
                }
                if (error == "") {
                    if (companydetails === null) {
                        var company = new Company();
                        company.name = req.body.cname;
                        company.details = req.body.cdesc;
                        company.save(function(err) {
                            if (err) {
                                error = "Something went wrong";
                            } else {
                                error = "Company added sucessfully!";
                            }

                        });
                    } else {
                        error = __n("alreadyexistsmsg")
                    }
                }
            });
        }
    }
    // rendering the page
    $data = {
        type: "su",
        title: "Add New Company",
        uuid: req.session.uuid,
        error: error
    };
    res.render('./popups/addcompany', $data);
});

function renderUserPage(uuid, error, res) {	
    Company.find({}, function(err, companies) {
        $data = {
            type: "su",
            title: "Add new user",
            uuid: uuid,
            error: error,
            companylist: companies,
            roles : settings.roles,
            data: {}
        };
        res.render('./popups/adduser', $data);
    });
}
module.exports = router;
