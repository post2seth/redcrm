var express = require('express');
var router = express.Router();
var Users = require('../dbmodels/users.js');
var Company = require('../dbmodels/company.js');
var md5 = require('js-md5');

var logger = require('winston');

router.get('/logout', function(req, res, next) {
    req.session.destroy(function(err) {
        if (err) {
            res.redirect('/error');
        } else {
            res.redirect('/');
        }
    });
    return;
});
/* GET home page. */
router.get('/', function(req, res, next) {
    Company.findOne(function(err, comp) {
        if (comp.length <= 0) {
            var company = new Company();
            company.name = "redcrm";
            company.details = "Best user CRM";

            company.save(function(err) {
                if (err) return handleError(err);
            });

            var user = new Users();
            user.username = "admin";
            user.password = md5("admin@123");
            user.role = 0;
            user.company_id = company._id;
            user.save(function(err) {
                if (err) {
                    return handleError(err)
                } else {
                    res.render('index', { title: __n("welcomemsg"), error: ""});
                }

            });
        } else {
            res.render('index', { title: __n("welcomemsg"), error: ""});
        }
    })

});

router.post('/', function(req, res, next) {
    if ((req.body.username === "" || req.body.password === "") || req.body.action === undefined) {
        res.render('index', { title: __n("emptyfields"), error: __n("emptyfields")});
        return;
    }

    if (req.body.action.toString().toLowerCase() == "login") {
        Users.findOne({ username: req.body.username, password: md5(req.body.password) }, function(err, userdetails) {
            if (err) {
                res.redirect("/error");
                return;
            }
            if (userdetails !== null) {
                req.session.authorized = true;
                req.session.uuid = userdetails.username;
                req.session.role = userdetails.role;
                req.session.__cid = userdetails.company_id;

                switch (userdetails.role) {
                    case 0:
                        res.redirect("/su");
                        break;
                    case 1:
                        res.redirect("/adm");
                        break;
                    default:
                        res.redirect("/usr");
                        break;
                }
                return;
            } else {
                res.render('index', { title: __n("incorrectcredential"), error: __n("incorrectcredential") });
                return;
            }

        });
    }
});

module.exports = router;
