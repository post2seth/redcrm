var express = require('express');
var router = express.Router();
var Users = require('../dbmodels/users.js');
var Company = require('../dbmodels/company.js');
var md5 = require('js-md5');

var logger = require('winston');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('forget', { title: __n("welcomemsg"), error: "" });
});

router.post('/', function(req, res, next) {
    if (req.body.email === "" || req.body.action === undefined) {
        res.render('forget', { title: __n("emptyfields"), error: __n("emptyfields") });
        return;
    }

    if (req.body.action.toString().toLowerCase() == "send") {
        Users.findOne({ username: req.body.email}, function(err, userdetails) {
            if (err) {
                res.redirect("/error");
                return;
            }
            if (userdetails !== null) {
            	res.render('forget', { title: __n("detailsend"), error:"" });
                return;
            } else {
                res.render('forget', { title: __n("usernotexists"), error: __n("usernotexists") });
                return;
            }
        });
    }
});

module.exports = router;
