var express = require('express');
var router = express.Router();
var Users = require('../dbmodels/users.js');
var Company = require('../dbmodels/company.js');
var md5 = require('js-md5');

var logger = require('winston');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('signup', { title: __n("welcomemsg"), error: "" });
});

router.post('/', function(req, res, next) {
    if ((req.body.username === "" || req.body.password === "") || req.body.action === undefined) {
        res.render('signup', { title: __n("emptyfields"), error: __n("emptyfields") });
        return;
    }

    if (req.body.action.toString().toLowerCase() == "signup") {
        Users.findOne({ username: req.body.username, password: md5(req.body.password) }, function(err, userdetails) {
            if (err) {
                res.redirect("/error");
                return;
            }
            if (userdetails === null) {
                if (req.body.cname !== "" && req.body.cdesc !== "") {
                    var company = new Company();
                    company.name = req.body.cname;
                    company.details = req.body.cdesc;
                    company.save(function(err) {
                        if (err) {
                            res.redirect("/error");
                            return;
                        }
                    });
                    var user = new Users();
                    user.username = req.body.username;
                    user.password = md5(req.body.password);
                    user.role = 6;
                    user.company_id = company._id;
                    user.save(function(err) {
                        if (err) {
                            res.redirect("/error");
                            return;
                        } else {
                            res.render('signup', { title: __n("welcomemsg"), error: "" });
                        }

                    });
                } else {
                    res.render('signup', { title: __n("emptyfields"), error: __n("emptyfields") });
                    return;
                }
            } else {
                res.render('signup', { title: __n("alreadyexiststitle"), error: __n("alreadyexistsmsg") });
                return;
            }
        });
    }
});

module.exports = router;
